from django.shortcuts import render
from rest_framework import viewsets
from django.contrib.auth.models import User
from .models import RegisterModel, ProductModel, OrderProductModel, OrderModel
# from rest_framework.decorators import api_view
from django.contrib.auth.forms import UserCreationForm
from .serializers import LoginSerializer, RegisterSerializer, ProductSerializer, OrderSerializer,OrderProductsSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


# Create your views here.
class LoginViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = LoginSerializer
    def get(self, request, id):
        return self.list(request, id)

    def post(self, request):
        response = 'Created'
        return render(self.create(request), {response: 'response'})


# @api_view(['POST'])
class RegisterViewSet(viewsets.ModelViewSet):
    queryset = RegisterModel.objects.all()
    serializer_class = RegisterSerializer

    def post(self, request):
        return render(self.request)




class ProductListViewSet(viewsets.ModelViewSet):
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = OrderModel.objects.all()
    serializer_class = OrderSerializer


class OrderProductViewSet(viewsets.ModelViewSet):
    queryset = OrderProductModel.objects.all()
    serializer_class = OrderProductsSerializer
