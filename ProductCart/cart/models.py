from django.db import models
from django.contrib.auth.models import User

class RegisterModel(models.Model):
    Username=models.CharField(max_length=50)
    Password=models.CharField(max_length=15)
    Confirm_Pwd=models.CharField(max_length=15,null=True)
    Email_id=models.EmailField()

class ProductModel(models.Model):
    Title_of_product = models.CharField(max_length=20)
    Description_of_product = models.CharField(max_length=100)
    Link_of_image = models.URLField()
    Price_of_product= models.DecimalField(max_digits=8, decimal_places=2)
    Product_created_on = models.DateField(auto_now=True)
    Product_updated_on = models.DateField()

    def __str__(self):
        return f"{self.Title_of_product} -{self.Description_of_product} - {self.Price_of_product} - {self.Product_created_on} - {self.Product_updated_on}"


class OrderModel(models.Model):
    new_order = 'New'
    paid_order = 'Paid'
    status_of_product = [
        (new_order, 'New'),
        (paid_order, 'Paid'),
    ]
    payment_selection = 'Cash On Delivery'
    mode_of_payment = [
        (payment_selection, 'Cash On Delivery')
    ]
    Status_of_product = models.CharField(max_length=16,choices=status_of_product)
    Payment = models.CharField(max_length=16,choices=mode_of_payment, default=payment_selection)
    Total_amount = models.DecimalField(max_digits=10, decimal_places=2)
    Order_placed_on= models.DateField(auto_now=True)
    Order_updated_on = models.DateField()
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.Status_of_product} - {self.Payment} - {self.Total_amount} - {self.Order_placed_on} - {self.Order_updated_on}"


class OrderProductModel(models.Model):
    Order = models.ForeignKey(OrderModel, on_delete=models.CASCADE)
    Product = models.ForeignKey(ProductModel, on_delete=models.CASCADE)
    Quantity = models.IntegerField(default=1)
    Price = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return f"{self.Order} - {self.Product} - {self.Quantity} - {self.Price}"