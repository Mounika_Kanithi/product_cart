from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.contrib.auth.forms import UserCreationForm
from .models import OrderModel, OrderProductModel, ProductModel,RegisterModel

class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True, 'required': True }}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user

class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegisterModel

        fields='__all__'
        extra_kwargs = {'password': {'write_only': True, 'required': True},'conform_password':{'write_only': True, 'required': True}}

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductModel
        fields = '__all__'

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model=OrderModel
        fields='__all__'

class OrderProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model= OrderProductModel
        fields='__all__'