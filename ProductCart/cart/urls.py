from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from rest_framework import routers
from .views import LoginViewSet, RegisterViewSet, ProductListViewSet, OrderProductViewSet, OrderViewSet

router = routers.DefaultRouter()
router.register('users', LoginViewSet)
router.register('register', RegisterViewSet)
router.register('list', ProductListViewSet)
router.register('orderby', OrderProductViewSet)
router.register('order', OrderViewSet)

urlpatterns = [
    path('', include(router.urls)),
]